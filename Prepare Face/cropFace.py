import numpy as np
import cv2
import os
from os import walk

fullPath = os.path.realpath(__file__)
localPath = os.path.dirname(fullPath)
folderPath = localPath + '/facesToCrop'
files = []
for (dirpath, dirnames, filenames) in walk(folderPath):
    files.extend(filenames)
    break


face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
myFileIndex = 1
width = 92
height = 112
personName = 'kevin'

def cropImage( imageFileName, originalFileName ):
    global myFileIndex
    global localPath
    img = cv2.imread(imageFileName)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    if(faces == ()):
        print('Can\'t Crop Picture ' + originalFileName)
    for (x,y,w,h) in faces:
        originalWidth = w
        originalHeight = h
        widhtPadding = w * .2
        heightPadding = h * .2
		
        x = x - widhtPadding
        y = y - heightPadding
        w = w + widhtPadding + widhtPadding
        h = h + heightPadding + heightPadding
        if(x < 0):
            x = 0
        if(y < 0):
            y = 0
        if(w < originalWidth):
            w = originalWidth
        if(h < originalHeight):
            h = originalHeight
		
		
        crop_img = img[y: y+h, x: x+w]
        crop_img2 = cv2.resize(crop_img, (width, height))
        fileName = str(myFileIndex) + '.png'
        myFileIndex = int(myFileIndex) + 1
        cv2.imwrite(localPath + '\croppedFaces\\' + fileName, crop_img2)
        print('Saving Picture ' + originalFileName + ' as ' + str(myFileIndex))
    return


for file in files:
    cropImage(folderPath + '/' + file, file)
