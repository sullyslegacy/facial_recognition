import numpy as np
import cv2


face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
width = 92
height = 112


#Combines getFace and cropFace
def getCroppedFace( imageFileName ):
	face = getFace(imageFileName)
	return cropFace(face)
	

#Return a users face, greyed out from the specified image
def getFace( imageFileName ):
    normalImage = cv2.imread(imageFileName)
    grayImage = cv2.cvtColor(normalImage, cv2.COLOR_BGR2GRAY)

    imageOfFace = face_cascade.detectMultiScale(grayImage, 1.3, 5)

    if(imageOfFace == ()):
        print('Can\'t Crop Picture ' + imageFileName)
	return imageOfFace
	

#Crop image of face to desired size
def cropFace(imageOfFace):
    for (x, y, w, h) in imageOfFace:
        originalWidth = w
        originalHeight = h
        widhtPadding = w * .2
        heightPadding = h * .2
		
        x = x - widhtPadding
        y = y - heightPadding
        w = w + widhtPadding
        h = h + heightPadding
        if(x < 0):
            x = 0
        if(y < 0):
            y = 0
        if(w < originalWidth):
            w = originalWidth
        if(h < originalHeight):
            h = originalHeight
		
        croppedImageDimensions = normalImage[y: y+h, x: x+w]
        croppedImage = cv2.resize(croppedImageDimensions, (width, height))
	return croppedImage