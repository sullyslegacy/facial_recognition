#import helpers modules
from helpers.util import read_images
from helpers.model import EigenfacesModel


faceNames = []
facePredictionModel = []

#Read images in filePath
#Compute the eigenfaces model from these images
def trainModel(trainingPath):
	global faceNames
	global facePredictionModel
	[xTrain, yTrain, faceNames] = read_images(trainingPath)
	facePredictionModel = EigenfacesModel(xTrain[1:], yTrain[1:])


#Take in an image
#From model predict most similar face
def predict(imageToPredict):
	global faceNames
	global facePredictionModel
	predictedFaceName = ''
	try:
		predictedFace = facePredictionModel.predict(imageToPredict)
		predictedFaceName = faceNames[ predictedFace ]
	except Exception: 
		predictedFaceName = 'No Face Found'
	return predictedFaceName
