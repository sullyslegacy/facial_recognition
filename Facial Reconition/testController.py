import predictFace as predicter
import imageController as imageController


#Train Model
print('Training Model')
predicter.trainModel('C:/workspaces/TestWorkSpace/Raspberry/Facial Reconition/training')

#Get our image of a face we want to identify
print('Cropping Image')
image = imageController.getImageFromFile('C:/workspaces/TestWorkSpace/Raspberry/Prepare Face/croppedFaces/5.png')
[faceFound, faceToPredict] = imageController.getCroppedFace(image)

#Predict who this is
print('Make Prediction of Face')
prediction = predicter.predict(faceToPredict);
print(prediction)
