import time
from time import gmtime, strftime
import cv2
import predictFace as predicter
import imageController as imageController


#Train Model
print('Training Model')
predicter.trainModel('C:/workspaces/TestWorkSpace/Raspberry/Facial Reconition/training')
print('Model Trained')

#cv2.namedWindow("preview")
vc = cv2.VideoCapture(0)	# 0 -> index of camera

if vc.isOpened(): # try to get the first image
    errors, image = vc.read()
else:
    errors = False


#capture = cv2.CaptureFromCAM(0)
while errors:
    #cv2.imshow("preview", image)
    errors, image = vc.read()
    #key = cv2.waitKey(20)
    time.sleep( 1 )
    #fileName = 'test' + strftime("%Y%m%d_%H%M%S", gmtime()) + '.png'
    #cv2.imwrite(fileName, image)		
    #print fileName
    #Get our image of a face we want to identify
    [faceFound, faceToPredict] = imageController.getCroppedFace(image)

    #Predict who this is
    if faceFound == 1:
        prediction = predicter.predict(faceToPredict)
        print(prediction)
    else:
        print('No Face Found')

    #if key == 27: # exit on ESC
    #    break

cv2.destroyWindow("preview")




