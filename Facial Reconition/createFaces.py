import time
from time import gmtime, strftime
import os
from os import walk
import cv2
import imageController as imageController

#Get Path and create new directory for images
fullPath = os.path.realpath(__file__)
localPath = os.path.dirname(fullPath) + '\\newFace\\' + strftime("%Y_%m_%d_%H_%M_%S", gmtime())
if not os.path.exists(localPath):
    os.makedirs(localPath)
localPath = localPath + '\\'
	
	
cv2.namedWindow("preview")
vc = cv2.VideoCapture(0)	# 0 -> index of camera

if vc.isOpened(): # try to get the first image
    errors, image = vc.read()
else:
    errors = False

counter = 0
while errors:
    cv2.imshow("preview", image)
    errors, image = vc.read()
    time.sleep( .1 )
	
    #Get our image of a face we want to identify
    [faceFound, faceToPredict] = imageController.getCroppedFace(image)
    if faceFound == 1:
        counter = counter + 1
        fileName = localPath + str(counter) + '.png'
        cv2.imwrite(fileName, faceToPredict)		
        print('Saved ' + fileName)
    else:
        print('Didn\t find a face')

    if counter >= 30:
        break

cv2.destroyWindow("preview")




