import numpy as np
import cv2

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
width = 92
height = 112

#Example Run
#test = getCroppedFace('C:/workspaces/TestWorkSpace/Raspberry/Prepare Face/croppedFaces/5.png')

def getImageFromFile( imageFileName ):
    originalImage = cv2.imread(imageFileName)
    return originalImage

	
#Combines getFace and cropFace
def getCroppedFace( image ):
    [faceFound, originalImage, grayImage, imageOfGreyFace] = getFace(image)
    croppedFace = cropFace(grayImage, imageOfGreyFace)
    return [faceFound, croppedFace]
	

#Return a users face, greyed out from the specified image
def getFace( originalImage ):
    grayImage = cv2.cvtColor(originalImage, cv2.COLOR_BGR2GRAY)
    #cv2.CascadeClassifier.detectMultiScale(image, rejectLevels, levelWeights
    #if rejectLevels set high get better face
    imageOfGreyFace = face_cascade.detectMultiScale(grayImage, 1.3, 5)

    faceFound = 1
    if(imageOfGreyFace == ()):
        faceFound = 0
    return [faceFound, originalImage, grayImage, imageOfGreyFace]

	
#Crop image of face to desired size
def cropFace(imageToCrop, imageOfFace):
    for (x, y, w, h) in imageOfFace:
        originalWidth = w
        originalHeight = h
        widhtPadding = w * .2
        heightPadding = h * .2
		
        x = x - widhtPadding
        y = y - heightPadding
        w = w + widhtPadding
        h = h + heightPadding
        if(x < 0):
            x = 0
        if(y < 0):
            y = 0
        if(w < originalWidth):
            w = originalWidth
        if(h < originalHeight):
            h = originalHeight
		
        croppedImageDimensions = imageToCrop[y: y+h, x: x+w]
        croppedImage = cv2.resize(croppedImageDimensions, (width, height))
        return croppedImage
