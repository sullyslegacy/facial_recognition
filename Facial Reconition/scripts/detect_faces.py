import os
from os import walk

# import numpy and matplotlib colormaps
import numpy as np
# import helpers modules
from helpers.util import read_images
from helpers.model import EigenfacesModel



fullPath = os.path.realpath(__file__)
localPath = os.path.dirname(fullPath)
localPath = localPath.replace('scripts', '')

trainingPath = localPath + 'training'
testPath = localPath + 'test'

    
# read images
[x, y, trainingNames] = read_images(trainingPath)
# compute the eigenfaces model
model = EigenfacesModel(x[1:], y[1:])

# read images
[xTest, yTest, testingNames] = read_images(testPath)

#i = 0
#while i < 409:
#	print "expected =", y[i], "/", "predicted =", model.predict(x[i])
#	i = i + 1

# get a prediction for the first observation
#print "expected =", y[0], "/", "predicted =", model.predict(x[0])
#print "expected =", y[11], "/", "predicted =", model.predict(x[11])
#print "expected =", y[22], "/", "predicted =", model.predict(x[22])
#print "expected =", y[3], "/", "predicted =", model.predict(x[3])
#print "expected =", y[4], "/", "predicted =", model.predict(x[4])

def predict(index):
	global trainingNames
	global testingNames
	global xTest
	global yTest
	
	xExpected = trainingNames[model.predict(xTest[index])]
	yExpected = testingNames[yTest[index]]
	print "expected =", yExpected, "/", "predicted =", xExpected

index = 0
for test in xTest:
	predict(index)
	index = index + 1
