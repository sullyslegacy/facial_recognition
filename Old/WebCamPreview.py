import cv2

cv2.namedWindow("preview")
vc = cv2.VideoCapture(0)

if vc.isOpened(): # try to get the first frame
    rval, frame = vc.read()
else:
    rval = False


#capture = cv2.CaptureFromCAM(0)
while rval:
    #errors, img = vc.read()
    cv2.imshow("preview", frame)
    rval, frame = vc.read()
    key = cv2.waitKey(20)
    if key == 27: # exit on ESC
        #cv2.imwrite('test.png',img)
        break

cv2.destroyWindow("preview")
