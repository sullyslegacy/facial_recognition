import sys
# append tinyfacerec to module search path
sys.path.append("..")
# import numpy and matplotlib colormaps
import numpy as np
# import tinyfacerec modules
from tinyfacerec.util import read_images
from tinyfacerec.model import EigenfacesModel

imagePath = 'C:/Users/ksulliva/OneDrive/Documents/Raspberry/Python/Detection/att_faces'
testPath = 'C:/Users/ksulliva/OneDrive/Documents/Raspberry/Python/Detection/test_faces'
    
# read images
[X,y] = read_images(imagePath)
# compute the eigenfaces model
model = EigenfacesModel(X[1:], y[1:])

# read images
[Xtest,ytest] = read_images(testPath)

#i = 0
#while i < 409:
#	print "expected =", y[i], "/", "predicted =", model.predict(X[i])
#	i = i + 1

# get a prediction for the first observation
#print "expected =", y[0], "/", "predicted =", model.predict(X[0])
#print "expected =", y[11], "/", "predicted =", model.predict(X[11])
#print "expected =", y[22], "/", "predicted =", model.predict(X[22])
#print "expected =", y[3], "/", "predicted =", model.predict(X[3])
#print "expected =", y[4], "/", "predicted =", model.predict(X[4])

print ytest
print "expected =", ytest[0], "/", "predicted =", model.predict(Xtest[0])
print "expected =", ytest[1], "/", "predicted =", model.predict(Xtest[1])
print "expected =", ytest[2], "/", "predicted =", model.predict(Xtest[2])
print "expected =", ytest[3], "/", "predicted =", model.predict(Xtest[3])
print "expected =", ytest[4], "/", "predicted =", model.predict(Xtest[4])
