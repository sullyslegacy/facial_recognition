import os
from os import walk

fullPath = os.path.realpath(__file__)
localPath = os.path.dirname(fullPath)
folderPath = localPath + '/facesToCrop'
files = []
for (dirpath, dirnames, filenames) in walk(folderPath):
    files.extend(filenames)
    break

for file in files:
	print(folderPath + "/" + file)