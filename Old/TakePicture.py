import cv2

cv2.namedWindow("preview")
vc = cv2.VideoCapture(0)	# 0 -> index of camera

if vc.isOpened(): # try to get the first image
    errors, image = vc.read()
else:
    errors = False


#capture = cv2.CaptureFromCAM(0)
while errors:
    cv2.imshow("preview", image)
    errors, image = vc.read()
    key = cv2.waitKey(20)
    if key == 32: # save on space
        cv2.imwrite('test.png',image)		
    elif key == 27: # exit on ESC
        break

cv2.destroyWindow("preview")
