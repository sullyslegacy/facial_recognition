import os
import sys
import cv2
import numpy as np


if __name__=='__main__':
    print "Face matching"
    if len(sys.argv) < 3:
            print "USAGE: matchfaces.py img1 img2"
            sys.exit()
    X= []
    x1 = []
    print sys.argv[2]
    im1 = cv2.imread(sys.argv[1],cv2.IMREAD_GRAYSCALE)
    im2 = cv2.imread(sys.argv[2],cv2.IMREAD_GRAYSCALE)
    model = cv2.createEigenFaceRecognizer()

    X.append(np.asarray(im1, dtype=np.uint8))
    X.append(np.asarray(im1, dtype=np.uint8))
    x1.append(0)
    x1.append(0)
    xlabels = np.asarray(x1, dtype=np.int32)
    model.train(X, xlabels)
    print model
    [p_label, p_confidence] = model.predict(np.asarray(im2, dtype=np.uint8))
    print "Done"
    print p_label, p_confidence
    print model.getMat("eigenvalues")