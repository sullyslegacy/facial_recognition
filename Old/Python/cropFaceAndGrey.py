import numpy as np
import cv2

face_cascade = cv2.CascadeClassifier('Detection\haarcascade_frontalface_default.xml')

imageToRead = 'C:\Users\ksulliva\Pictures\Other\Kev.png';

img = cv2.imread(imageToRead)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

faces = face_cascade.detectMultiScale(gray, 1.3, 5)

for (x,y,w,h) in faces:
	widhtPadding = w * .2
	heightPadding = h * .2
	x = x - widhtPadding
	y = y - heightPadding
	w = w + widhtPadding + widhtPadding
	h = h + heightPadding + heightPadding

	#cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
	crop_img = img[y: y+h, x: x+w]
	#cv2.imshow("cropped", crop_img)
	#cv2.waitKey(0)

	crop_img2 = cv2.resize(crop_img, (92, 112))
	cv2.imshow("cropped", crop_img2)
	cv2.waitKey(0)
	cv2.imwrite('newImage.png',crop_img2)

cv2.destroyAllWindows()
